<?php
  class Berita extends CI_Model{

    public function __construct()
    {
      parent::__construct();
      $this->load->database();
    }

    public function getData()
    {
      $query = $this->db->get('berita');
      return $query->result_array();

    }

  }
?>
