<?php
  class Rens extends CI_Model{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    // public function getData()
    // {
    //     $this->db->join('parameters_values b','b.id_parameter = a.id');
    //     $query = $this->db->get('parameters a');
    //     return $query->result_object();
    // }
    
    //-------------------------------------------- PRIOR ---------------------------------------------------//
    public function getPriorCrowded()
    {
        // $query = $this->db->get_where('training_set',array('sales','crowded'));
        $query = $this->db->query("select count(*) as result from training_set where sales = 'crowded'");
        return $query->row();
    }
    
    public function getPriorQuiet()
    {
        // $query = $this->db->get_where('training_set',array('sales','Quiet'));
        $query = $this->db->query("select count(*) as result from training_set where sales = 'quiet'");
        return $query->row();
    }

    //-------------------------------------------- EVIDENCE WEEK ---------------------------------------------------//

    public function getWeekendCrowded()
    {
        // $query = $this->db->get_where('training_set',array('sales','Quiet'));
        $query = $this->db->query("select count(*) as result from training_set where week = 'weekend' and sales = 'crowded'");
        return $query->row();
    }

    public function getWeekendQuiet()
    {
        // $query = $this->db->get_where('training_set',array('sales','Quiet'));
        $query = $this->db->query("select count(*) as result from training_set where week = 'weekend' and sales = 'quiet'");
        return $query->row();
    }

    public function getWeekdayCrowded()
    {
        // $query = $this->db->get_where('training_set',array('sales','Quiet'));
        $query = $this->db->query("select count(*) as result from training_set where week = 'weekday' and sales = 'crowded'");
        return $query->row();
    }

    public function getWeekdayQuiet()
    {
        // $query = $this->db->get_where('training_set',array('sales','Quiet'));
        $query = $this->db->query("select count(*) as result from training_set where week = 'weekday' and sales = 'quiet'");
        return $query->row();
    }

 //-------------------------------------------- EVIDENCE WEATHER ---------------------------------------------------//

    public function getSunnyCrowded()
    {
        // $query = $this->db->get_where('training_set',array('sales','Quiet'));
        $query = $this->db->query("select count(*) as result from training_set where weather = 'Sunny' and sales = 'crowded'");
        return $query->row();
    }

    public function getSunnyQuiet()
    {
        // $query = $this->db->get_where('training_set',array('sales','Quiet'));
        $query = $this->db->query("select count(*) as result from training_set where weather = 'Sunny' and sales = 'quiet'");
        return $query->row();
    }

    public function getRainyCrowded()
    {
        // $query = $this->db->get_where('training_set',array('sales','Quiet'));
        $query = $this->db->query("select count(*) as result from training_set where weather = 'Rainy' and sales = 'crowded'");
        return $query->row();
    }

    public function getRainyQuiet()
    {
        // $query = $this->db->get_where('training_set',array('sales','Quiet'));
        $query = $this->db->query("select count(*) as result from training_set where weather = 'Rainy' and sales = 'quiet'");
        return $query->row();
    }

     //-------------------------------------------- EVIDENCE HOLIDAY ---------------------------------------------------//

     public function getYesCrowded()
    {
        // $query = $this->db->get_where('training_set',array('sales','Quiet'));
        $query = $this->db->query("select count(*) as result from training_set where holiday = 'Yes' and sales = 'crowded'");
        return $query->row();
    }

    public function getYesQuiet()
    {
        // $query = $this->db->get_where('training_set',array('sales','Quiet'));
        $query = $this->db->query("select count(*) as result from training_set where holiday = 'Yes' and sales = 'quiet'");
        return $query->row();
    }

    public function getNoCrowded()
    {
        // $query = $this->db->get_where('training_set',array('sales','Quiet'));
        $query = $this->db->query("select count(*) as result from training_set where holiday = 'No' and sales = 'crowded'");
        return $query->row();
    }

    public function getNoQuiet()
    {
        // $query = $this->db->get_where('training_set',array('sales','Quiet'));
        $query = $this->db->query("select count(*) as result from training_set where holiday = 'No' and sales = 'quiet'");
        return $query->row();
    }

     //-------------------------------------------- EVIDENCE HOLIDAY ---------------------------------------------------//

     public function getEarlyCrowded()
    {
        // $query = $this->db->get_where('training_set',array('sales','Quiet'));
        $query = $this->db->query("select count(*) as result from training_set where month = 'Early' and sales = 'crowded'");
        return $query->row();
    }

    public function getEarlyQuiet()
    {
        // $query = $this->db->get_where('training_set',array('sales','Quiet'));
        $query = $this->db->query("select count(*) as result from training_set where month = 'Early' and sales = 'quiet'");
        return $query->row();
    }

    public function getEndCrowded()
    {
        // $query = $this->db->get_where('training_set',array('sales','Quiet'));
        $query = $this->db->query("select count(*) as result from training_set where month = 'End' and sales = 'crowded'");
        return $query->row();
    }

    public function getEndQuiet()
    {
        // $query = $this->db->get_where('training_set',array('sales','Quiet'));
        $query = $this->db->query("select count(*) as result from training_set where month = 'End' and sales = 'quiet'");
        return $query->row();
    }
  }
?>
